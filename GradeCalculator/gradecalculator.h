#ifndef GRADECALCULATOR_H
#define GRADECALCULATOR_H

#include <QMainWindow>

namespace Ui {
class GradeCalculator;
}

class GradeCalculator : public QMainWindow
{
    Q_OBJECT

public:
    explicit GradeCalculator(QWidget *parent = 0);
    ~GradeCalculator();

public slots:
    void computeGrade();
    void reset();

private:
    Ui::GradeCalculator *ui;
};

#endif // GRADECALCULATOR_H
