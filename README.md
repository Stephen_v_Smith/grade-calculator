# Grade Calculator #
* Using Qt, we are trying to create a user interface for a grade calculator
* The calculator uses prescribed weights for all of the different assignments as indicated on CCLE
* There are two possible schemes for the grades to be calculated (Schema A and Schema B)
* There are two classes to calculate your grades as well (PIC 10B and PIC 10C)
* Just press the button "Calculate" and you're good to go!