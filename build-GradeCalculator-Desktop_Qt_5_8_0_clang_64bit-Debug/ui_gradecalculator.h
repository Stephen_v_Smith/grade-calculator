/********************************************************************************
** Form generated from reading UI file 'gradecalculator.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_GRADECALCULATOR_H
#define UI_GRADECALCULATOR_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QComboBox>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QMenu>
#include <QtWidgets/QMenuBar>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QRadioButton>
#include <QtWidgets/QSlider>
#include <QtWidgets/QSpinBox>
#include <QtWidgets/QStatusBar>
#include <QtWidgets/QToolBar>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_GradeCalculator
{
public:
    QWidget *centralWidget;
    QSlider *hw1;
    QSlider *hw2;
    QSlider *hw3;
    QSlider *hw4;
    QSpinBox *hw1c;
    QSpinBox *hw2c;
    QSpinBox *hw3c;
    QSpinBox *hw4c;
    QSlider *hw5;
    QSpinBox *hw5c;
    QSlider *hw6;
    QSpinBox *hw6c;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLabel *label_8;
    QSlider *hw7;
    QSpinBox *hw7c;
    QSlider *hw8;
    QSpinBox *hw8c;
    QLabel *label_9;
    QComboBox *courses;
    QSlider *midterm1;
    QLabel *label_10;
    QSlider *midterm2;
    QLabel *label_11;
    QLabel *label_12;
    QSlider *final_2;
    QRadioButton *schemaA;
    QRadioButton *schemaB;
    QLabel *label_13;
    QLabel *Overall;
    QSpinBox *midterm1c;
    QSpinBox *midterm2c;
    QSpinBox *finalc;
    QPushButton *pushButton;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;
    QMenuBar *menuBar;
    QMenu *menuGrade_Calculator;

    void setupUi(QMainWindow *GradeCalculator)
    {
        if (GradeCalculator->objectName().isEmpty())
            GradeCalculator->setObjectName(QStringLiteral("GradeCalculator"));
        GradeCalculator->resize(665, 495);
        centralWidget = new QWidget(GradeCalculator);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        hw1 = new QSlider(centralWidget);
        hw1->setObjectName(QStringLiteral("hw1"));
        hw1->setGeometry(QRect(60, 40, 171, 22));
        hw1->setMaximum(100);
        hw1->setOrientation(Qt::Horizontal);
        hw2 = new QSlider(centralWidget);
        hw2->setObjectName(QStringLiteral("hw2"));
        hw2->setGeometry(QRect(60, 90, 171, 22));
        hw2->setMaximum(100);
        hw2->setOrientation(Qt::Horizontal);
        hw3 = new QSlider(centralWidget);
        hw3->setObjectName(QStringLiteral("hw3"));
        hw3->setGeometry(QRect(60, 140, 171, 22));
        hw3->setMaximum(100);
        hw3->setOrientation(Qt::Horizontal);
        hw4 = new QSlider(centralWidget);
        hw4->setObjectName(QStringLiteral("hw4"));
        hw4->setGeometry(QRect(60, 190, 171, 22));
        hw4->setMaximum(100);
        hw4->setOrientation(Qt::Horizontal);
        hw1c = new QSpinBox(centralWidget);
        hw1c->setObjectName(QStringLiteral("hw1c"));
        hw1c->setGeometry(QRect(240, 40, 48, 24));
        hw1c->setMaximum(100);
        hw2c = new QSpinBox(centralWidget);
        hw2c->setObjectName(QStringLiteral("hw2c"));
        hw2c->setGeometry(QRect(240, 90, 48, 24));
        hw2c->setMaximum(100);
        hw3c = new QSpinBox(centralWidget);
        hw3c->setObjectName(QStringLiteral("hw3c"));
        hw3c->setGeometry(QRect(240, 140, 48, 24));
        hw3c->setMaximum(100);
        hw4c = new QSpinBox(centralWidget);
        hw4c->setObjectName(QStringLiteral("hw4c"));
        hw4c->setGeometry(QRect(240, 190, 48, 24));
        hw4c->setMaximum(100);
        hw5 = new QSlider(centralWidget);
        hw5->setObjectName(QStringLiteral("hw5"));
        hw5->setGeometry(QRect(60, 240, 171, 22));
        hw5->setMaximum(100);
        hw5->setOrientation(Qt::Horizontal);
        hw5c = new QSpinBox(centralWidget);
        hw5c->setObjectName(QStringLiteral("hw5c"));
        hw5c->setGeometry(QRect(240, 240, 48, 24));
        hw5c->setMaximum(100);
        hw6 = new QSlider(centralWidget);
        hw6->setObjectName(QStringLiteral("hw6"));
        hw6->setGeometry(QRect(60, 290, 171, 22));
        hw6->setMaximum(100);
        hw6->setOrientation(Qt::Horizontal);
        hw6c = new QSpinBox(centralWidget);
        hw6c->setObjectName(QStringLiteral("hw6c"));
        hw6c->setGeometry(QRect(240, 290, 48, 24));
        hw6c->setMaximum(100);
        label = new QLabel(centralWidget);
        label->setObjectName(QStringLiteral("label"));
        label->setGeometry(QRect(10, 40, 31, 16));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QStringLiteral("label_2"));
        label_2->setGeometry(QRect(10, 90, 31, 16));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QStringLiteral("label_3"));
        label_3->setGeometry(QRect(10, 140, 31, 16));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QStringLiteral("label_4"));
        label_4->setGeometry(QRect(10, 190, 31, 16));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QStringLiteral("label_5"));
        label_5->setGeometry(QRect(10, 240, 31, 16));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QStringLiteral("label_6"));
        label_6->setGeometry(QRect(10, 290, 31, 16));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QStringLiteral("label_7"));
        label_7->setGeometry(QRect(10, 340, 31, 16));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QStringLiteral("label_8"));
        label_8->setGeometry(QRect(10, 390, 31, 16));
        hw7 = new QSlider(centralWidget);
        hw7->setObjectName(QStringLiteral("hw7"));
        hw7->setGeometry(QRect(60, 340, 171, 22));
        hw7->setMaximum(100);
        hw7->setOrientation(Qt::Horizontal);
        hw7c = new QSpinBox(centralWidget);
        hw7c->setObjectName(QStringLiteral("hw7c"));
        hw7c->setGeometry(QRect(240, 340, 48, 24));
        hw7c->setMaximum(100);
        hw8 = new QSlider(centralWidget);
        hw8->setObjectName(QStringLiteral("hw8"));
        hw8->setGeometry(QRect(60, 390, 171, 22));
        hw8->setMaximum(100);
        hw8->setOrientation(Qt::Horizontal);
        hw8c = new QSpinBox(centralWidget);
        hw8c->setObjectName(QStringLiteral("hw8c"));
        hw8c->setGeometry(QRect(240, 390, 48, 24));
        hw8c->setMaximum(100);
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QStringLiteral("label_9"));
        label_9->setGeometry(QRect(310, 10, 60, 16));
        courses = new QComboBox(centralWidget);
        courses->setObjectName(QStringLiteral("courses"));
        courses->setGeometry(QRect(380, 10, 211, 26));
        midterm1 = new QSlider(centralWidget);
        midterm1->setObjectName(QStringLiteral("midterm1"));
        midterm1->setGeometry(QRect(360, 70, 271, 22));
        midterm1->setMaximum(100);
        midterm1->setOrientation(Qt::Horizontal);
        label_10 = new QLabel(centralWidget);
        label_10->setObjectName(QStringLiteral("label_10"));
        label_10->setGeometry(QRect(460, 50, 111, 20));
        midterm2 = new QSlider(centralWidget);
        midterm2->setObjectName(QStringLiteral("midterm2"));
        midterm2->setGeometry(QRect(360, 170, 271, 22));
        midterm2->setMaximum(100);
        midterm2->setSingleStep(1);
        midterm2->setOrientation(Qt::Horizontal);
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QStringLiteral("label_11"));
        label_11->setGeometry(QRect(460, 150, 111, 20));
        label_12 = new QLabel(centralWidget);
        label_12->setObjectName(QStringLiteral("label_12"));
        label_12->setGeometry(QRect(460, 250, 111, 20));
        final_2 = new QSlider(centralWidget);
        final_2->setObjectName(QStringLiteral("final_2"));
        final_2->setGeometry(QRect(360, 270, 261, 22));
        final_2->setMaximum(100);
        final_2->setOrientation(Qt::Horizontal);
        schemaA = new QRadioButton(centralWidget);
        schemaA->setObjectName(QStringLiteral("schemaA"));
        schemaA->setGeometry(QRect(380, 350, 100, 20));
        schemaB = new QRadioButton(centralWidget);
        schemaB->setObjectName(QStringLiteral("schemaB"));
        schemaB->setGeometry(QRect(380, 380, 100, 20));
        label_13 = new QLabel(centralWidget);
        label_13->setObjectName(QStringLiteral("label_13"));
        label_13->setGeometry(QRect(490, 370, 91, 16));
        Overall = new QLabel(centralWidget);
        Overall->setObjectName(QStringLiteral("Overall"));
        Overall->setGeometry(QRect(590, 370, 60, 16));
        midterm1c = new QSpinBox(centralWidget);
        midterm1c->setObjectName(QStringLiteral("midterm1c"));
        midterm1c->setGeometry(QRect(370, 110, 261, 24));
        midterm1c->setMaximum(100);
        midterm2c = new QSpinBox(centralWidget);
        midterm2c->setObjectName(QStringLiteral("midterm2c"));
        midterm2c->setGeometry(QRect(370, 210, 261, 24));
        midterm2c->setMaximum(100);
        finalc = new QSpinBox(centralWidget);
        finalc->setObjectName(QStringLiteral("finalc"));
        finalc->setGeometry(QRect(370, 310, 261, 24));
        finalc->setMaximum(100);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));
        pushButton->setGeometry(QRect(500, 390, 113, 32));
        GradeCalculator->setCentralWidget(centralWidget);
        mainToolBar = new QToolBar(GradeCalculator);
        mainToolBar->setObjectName(QStringLiteral("mainToolBar"));
        GradeCalculator->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(GradeCalculator);
        statusBar->setObjectName(QStringLiteral("statusBar"));
        GradeCalculator->setStatusBar(statusBar);
        menuBar = new QMenuBar(GradeCalculator);
        menuBar->setObjectName(QStringLiteral("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 665, 22));
        menuGrade_Calculator = new QMenu(menuBar);
        menuGrade_Calculator->setObjectName(QStringLiteral("menuGrade_Calculator"));
        GradeCalculator->setMenuBar(menuBar);

        menuBar->addAction(menuGrade_Calculator->menuAction());

        retranslateUi(GradeCalculator);
        QObject::connect(hw1, SIGNAL(valueChanged(int)), hw1c, SLOT(setValue(int)));
        QObject::connect(hw1c, SIGNAL(valueChanged(int)), hw1, SLOT(setValue(int)));
        QObject::connect(hw2, SIGNAL(valueChanged(int)), hw2c, SLOT(setValue(int)));
        QObject::connect(hw2c, SIGNAL(valueChanged(int)), hw2, SLOT(setValue(int)));
        QObject::connect(hw3, SIGNAL(valueChanged(int)), hw3c, SLOT(setValue(int)));
        QObject::connect(hw3c, SIGNAL(valueChanged(int)), hw3, SLOT(setValue(int)));
        QObject::connect(hw4, SIGNAL(valueChanged(int)), hw4c, SLOT(setValue(int)));
        QObject::connect(hw4c, SIGNAL(valueChanged(int)), hw4, SLOT(setValue(int)));
        QObject::connect(hw5, SIGNAL(valueChanged(int)), hw5c, SLOT(setValue(int)));
        QObject::connect(hw5c, SIGNAL(valueChanged(int)), hw5, SLOT(setValue(int)));
        QObject::connect(hw6, SIGNAL(valueChanged(int)), hw6c, SLOT(setValue(int)));
        QObject::connect(hw6c, SIGNAL(valueChanged(int)), hw6, SLOT(setValue(int)));
        QObject::connect(hw7, SIGNAL(valueChanged(int)), hw7c, SLOT(setValue(int)));
        QObject::connect(hw7c, SIGNAL(valueChanged(int)), hw7, SLOT(setValue(int)));
        QObject::connect(hw8, SIGNAL(valueChanged(int)), hw8c, SLOT(setValue(int)));
        QObject::connect(hw8c, SIGNAL(valueChanged(int)), hw8, SLOT(setValue(int)));
        QObject::connect(midterm1, SIGNAL(valueChanged(int)), midterm1c, SLOT(setValue(int)));
        QObject::connect(midterm2, SIGNAL(valueChanged(int)), midterm2c, SLOT(setValue(int)));
        QObject::connect(midterm1c, SIGNAL(valueChanged(int)), midterm1, SLOT(setValue(int)));
        QObject::connect(midterm2c, SIGNAL(valueChanged(int)), midterm2, SLOT(setValue(int)));
        QObject::connect(final_2, SIGNAL(valueChanged(int)), finalc, SLOT(setValue(int)));
        QObject::connect(finalc, SIGNAL(valueChanged(int)), final_2, SLOT(setValue(int)));
        QObject::connect(courses, SIGNAL(currentIndexChanged(int)), GradeCalculator, SLOT(repaint()));

        QMetaObject::connectSlotsByName(GradeCalculator);
    } // setupUi

    void retranslateUi(QMainWindow *GradeCalculator)
    {
        GradeCalculator->setWindowTitle(QApplication::translate("GradeCalculator", "GradeCalculator", Q_NULLPTR));
        label->setText(QApplication::translate("GradeCalculator", "HW1", Q_NULLPTR));
        label_2->setText(QApplication::translate("GradeCalculator", "HW2", Q_NULLPTR));
        label_3->setText(QApplication::translate("GradeCalculator", "HW3", Q_NULLPTR));
        label_4->setText(QApplication::translate("GradeCalculator", "HW4", Q_NULLPTR));
        label_5->setText(QApplication::translate("GradeCalculator", "HW5", Q_NULLPTR));
        label_6->setText(QApplication::translate("GradeCalculator", "HW6", Q_NULLPTR));
        label_7->setText(QApplication::translate("GradeCalculator", "HW7", Q_NULLPTR));
        label_8->setText(QApplication::translate("GradeCalculator", "HW8", Q_NULLPTR));
        label_9->setText(QApplication::translate("GradeCalculator", "Course:", Q_NULLPTR));
        courses->clear();
        courses->insertItems(0, QStringList()
         << QApplication::translate("GradeCalculator", "Program in Computing 10B", Q_NULLPTR)
         << QApplication::translate("GradeCalculator", "Program in Computing 10C", Q_NULLPTR)
        );
        label_10->setText(QApplication::translate("GradeCalculator", "Midterm 1", Q_NULLPTR));
        label_11->setText(QApplication::translate("GradeCalculator", "Midterm 2", Q_NULLPTR));
        label_12->setText(QApplication::translate("GradeCalculator", "Final Exam", Q_NULLPTR));
        schemaA->setText(QApplication::translate("GradeCalculator", "Schema A", Q_NULLPTR));
        schemaB->setText(QApplication::translate("GradeCalculator", "Schema B", Q_NULLPTR));
        label_13->setText(QApplication::translate("GradeCalculator", "Overall Grade:", Q_NULLPTR));
        Overall->setText(QApplication::translate("GradeCalculator", "0.0", Q_NULLPTR));
        pushButton->setText(QApplication::translate("GradeCalculator", "Calculate", Q_NULLPTR));
        menuGrade_Calculator->setTitle(QApplication::translate("GradeCalculator", "Grade Calculator", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class GradeCalculator: public Ui_GradeCalculator {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_GRADECALCULATOR_H
