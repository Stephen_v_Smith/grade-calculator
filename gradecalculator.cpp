#include "gradecalculator.h"
#include "ui_gradecalculator.h"

GradeCalculator::GradeCalculator(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::GradeCalculator)
{
    ui->setupUi(this);
    QObject::connect(ui->pushButton,SIGNAL(clicked()),this,SLOT(computeGrade()));
    QObject::connect(ui->courses,SIGNAL(currentIndexChanged(int)),this,SLOT(reset()));
}

GradeCalculator::~GradeCalculator()
{
    delete ui;
}

void GradeCalculator::computeGrade()
{
    double totalHWPoints=700.0;
    int score = ui->hw1->value();
    int lowest=ui->hw1->value();

    score+=ui->hw2->value();
    if ((ui->hw2->value())<lowest)
        lowest=ui->hw2->value();

    score+=ui->hw3->value();
    if ((ui->hw3->value())<lowest)
        lowest=ui->hw3->value();

    score+=ui->hw4->value();
    if ((ui->hw4->value())<lowest)
        lowest=ui->hw4->value();

    score+=ui->hw5->value();
    if ((ui->hw5->value())<lowest)
        lowest=ui->hw5->value();

    score+=ui->hw6->value();
    if ((ui->hw6->value())<lowest)
        lowest=ui->hw6->value();

    score+=ui->hw7->value();
    if ((ui->hw7->value())<lowest)
        lowest=ui->hw7->value();

    score+=ui->hw8->value();
    if ((ui->hw8->value())<lowest)
        lowest=ui->hw8->value();

    score-=lowest;

    double hw_score=score/totalHWPoints;
    double hw_contribution=hw_score*25;

    double midterm1score=(ui->midterm1->value())/100.0;
    double midterm2score=(ui->midterm2->value())/100.0;
    double highMid;
    double fullscore=0.0;

    if (ui->schemaB->isChecked()==true) {
        if (midterm1score>midterm2score) highMid=midterm1score;
        else highMid=midterm2score;
        double midterm_contribution=highMid*30;
        double finalscore=(ui->final_2->value())/100.0;
        double finalcontribution=finalscore*44;

        fullscore = hw_contribution+midterm_contribution+finalcontribution;
    }
    else {
        double midterm2contribution=midterm2score*20.0;
        double midterm1contribution=midterm1score*20.0;
        double finalscore=(ui->final_2->value())/100.0;
        double finalcontribution=finalscore*35;

        fullscore = hw_contribution+midterm1contribution+midterm2contribution+finalcontribution;
    }
    ui->Overall->setText(QString::number(fullscore));
    return;
}

void GradeCalculator::reset()
{
    ui->hw1->setValue(0);
    ui->hw2->setValue(0);
    ui->hw3->setValue(0);
    ui->hw4->setValue(0);
    ui->hw5->setValue(0);
    ui->hw6->setValue(0);
    ui->hw7->setValue(0);
    ui->hw8->setValue(0);

    ui->midterm1->setValue(0);
    ui->midterm2->setValue(0);
    ui->final_2->setValue(0);

    ui->Overall->setText(QString::number(0.0));
}
